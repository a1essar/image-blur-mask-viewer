define('app', [
    'domReady',
    'jquery',
    'underscore',
    'utils',
    'image-blur-mask-viewer'
],  function ( domReady, $, _, utils, ImageBlurMaskViewer ) {
    'use strict';

    console.log('%cfile: app.js', 'color: #C2ECFF');

    /** private */
    var _this;
    var _defaults = {};

    /** constructor
     * @return {boolean}
     */
    function App(){
        console.log('%ctrace: App -> constructor', 'color: #ccc');

        _this = this;

        /** public */
        this.options = _.extend({

            },
            _defaults
        );

        utils.shims();

        var imageBlurMaskViewer = new ImageBlurMaskViewer();

        domReady(function () {
            console.log('%ctrace: App -> constructor -> domReady', 'color: #ccc');
        });
    }

    return App;
});