define('image-blur-mask-viewer', [
    'domReady',
    'jquery',
    'underscore',
    'StackBlur',
    'spin'
], function (domReady, $, _, StackBlur, Spinner) {
    'use strict';

    console.log('%cfile: image-blur-mask-viewer.js', 'color: #C2ECFF');

    var defaults = {
            elements: {
                'viewer': '[data-image-blur-mask-viewer]',
                'slides': '[data-image-blur-mask-slide]',
                'slide': '[data-image-blur-mask-slide].active',
                'canvas': '[data-image-blur-mask-slide].active [data-image-blur-mask-canvas]',
                'preloader': '[data-image-blur-mask-slide].active [data-image-blur-mask-preloader]',
                'preloaderDescription': '[data-image-blur-mask-slide].active [data-image-blur-mask-preloader-description]',
                'stub': '[data-image-blur-mask-slide].active [data-image-blur-mask-stub]'
            },
            $elements: {},
            original: {
                width: 1400,
                height: 700
            },
            spinner: {
                lines: 12,
                length: 24,
                width: 3,
                radius: 3,
                corners: 1,
                rotate: 0,
                direction: 1,
                color: '#fff',
                speed: 2,
                trail: 60,
                shadow: false,
                hwaccel: true,
                className: 'spinner',
                zIndex: 100500,
                top: '50%',
                left: '50%'
            }
        },

        canvas = [],
        context = [],
        images = [],
        bgPosition = {},

        initialize,
        createCanvas,
        domElementsUpdates,
        loadImages,
        start,
        resize,
        drawBgBlur,
        drawBg,
        drawMask,
        drawLens,
        animate;

    /** constructor */
    function Module() {
        console.log('%ctrace: imageBlurMaskViewer -> constructor', 'color: #ccc');

        domReady(function () {
            console.log('%ctrace: imageBlurMaskViewer -> constructor -> domReady', 'color: #ccc');

            domElementsUpdates();

            if (!defaults.$elements.viewer.length) {
                console.warn('%ctrace: imageBlurMaskViewer -> constructor -> domReady: not found dom elements', 'color: #ccc');
                return false;
            }

            defaults = _.extend(JSON.parse(defaults.$elements.slide.attr('data-image-blur-mask-slide')), defaults);

            console.log(defaults);

            initialize();
        });
    }

    initialize = function initialize() {
        start();

        loadImages(defaults.images, function(imgs) {
            images = imgs;

            defaults.$elements.slide.addClass('load');
            defaults.$elements.preloader.remove();
            defaults.$elements.preloaderDescription.remove();
            defaults.$elements.stub.remove();

            createCanvas();

            resize();
            drawBgBlur();
            drawBg();
            drawMask();
            drawLens();

            $(window).off('resize').on('resize', _.throttle(function(event) {
                resize(event);
                drawBgBlur();
                drawBg();
                drawMask();
                drawLens();
            }, 300));

            $(window).off('mousemove').on('mousemove', function(event) {
                animate(event);
            }).trigger('mousemove');
        });
    };

    animate = function animate(event) {
        console.log('%ctrace: imageBlurMaskViewer -> animate', 'color: #ccc');

        var clientX = event.clientX || 0.1,
            clientY = event.clientY || 0.1;

        bgPosition.offsetX = defaults.width / 100 * 2; /* parallax offset */
        bgPosition.offsetY = defaults.height / 100 * 2; /* parallax offset */

        if (clientX >= defaults.width) {
            clientX = defaults.width;
        }

        if (clientY >= defaults.height) {
            clientY = defaults.height;
        }

        bgPosition.sx = bgPosition.offsetX * clientX / defaults.width;
        bgPosition.sy = bgPosition.offsetY * clientY / defaults.height;
        bgPosition.dx = 0;
        bgPosition.dy = 0;
        bgPosition.dw = defaults.width;
        bgPosition.dh = defaults.height;

        drawBg(bgPosition);
        drawMask(bgPosition);
        drawLens(bgPosition);
    };

    resize = function resize() {
        console.log('%ctrace: imageBlurMaskViewer -> resize', 'color: #ccc');

        /*original factor*/
        var widthFactor = images.lens.width / defaults.original.width;

        defaults.ratio = images.bg.height / images.bg.width;
        defaults.width = parseInt(defaults.$elements.viewer.css('min-width'), 10) || $('body').width();
        defaults.height = defaults.width * defaults.ratio;

        defaults.lensRatio = images.lens.height / images.lens.width;
        defaults.lensWidth = defaults.width * widthFactor;
        defaults.lensHeight = defaults.lensWidth * defaults.lensRatio;

        defaults.lensLeftX = defaults.width / 2 - defaults.lensWidth / 2;
        defaults.lensLeftY = defaults.height * 0.21428571428571427; /* padding top from design: 150/700 */

        /* clear canvas */
        _.forEach(Object.keys(canvas), function(value) {
            canvas[value].width = defaults.width;
            canvas[value].height = defaults.height;
        });
    };

    createCanvas = function createCanvas() {
        console.log('%ctrace: imageBlurMaskViewer -> createCanvas', 'color: #ccc');

        _.forEach(defaults.$elements.canvas, function(value, key) {
            var name = defaults.$elements.canvas.eq(key).attr('data-image-blur-mask-canvas');

            canvas[name] = defaults.$elements.canvas[key];
            context[name] = canvas[name].getContext('2d');
        });
    };

    domElementsUpdates = function domElementsUpdates() {
        console.log('%ctrace: imageBlurMaskViewer -> domElementsUpdates', 'color: #ccc');

        _.forEach(defaults.elements, function(value, key) {
            defaults.$elements[key] = $(value);
        });
    };

    loadImages = function loadImages(imgs, callback) {
        console.log('%ctrace: imageBlurMaskViewer -> loadImages', 'color: #ccc');

        var loaded = 0,
            loadedImages = [];

        _.forEach(imgs, function (value, key) {
            var image = new Image();

            image.onload = function() {
                loadedImages[key] = this;

                if (loaded === Object.keys(imgs).length - 1) {
                    callback(loadedImages);
                }

                loaded++;
            };

            image.src = value;
        });
    };

    start = function start() {
        console.log('%ctrace: imageBlurMaskViewer -> start', 'color: #ccc');

        var spinner = new Spinner(defaults.spinner).spin();
        defaults.$elements.preloader.html(spinner.el);
    };

    drawBgBlur = function drawBgBlur(param) {
        console.log('%ctrace: imageBlurMaskViewer -> drawBgBlur', 'color: #ccc');

        param = param || {};

        param.offsetX = param.offsetX || defaults.width / 100;
        param.offsetY = param.offsetY || defaults.height / 100;

        param.sx = param.sx || param.offsetX;
        param.sy = param.sy || param.offsetY;
        param.sw = images.bg.width - param.offsetX;
        param.sh = images.bg.height - param.offsetY;
        param.dx = param.dx || 0;
        param.dy = param.dy || 0;
        param.dw = param.dw || defaults.width;
        param.dh = param.dh || defaults.height;

        context.blur.save();

        context.blur.drawImage(images.bg, param.sx, param.sy, param.sw, param.sh, param.dx, param.dy, param.dw, param.dh);
        StackBlur.stackBlurCanvasRGB('image-blur-mask-canvas', 0, 0, param.dw, param.dh, defaults.blur);

        context.blur.restore();

        context.bgs.save();

        context.bgs.drawImage(images.bg, param.sx, param.sy, param.sw, param.sh, param.dx, param.dy, param.dw, param.dh);

        context.bgs.restore();
    };

    drawBg = function drawBg(param) {
        console.log('%ctrace: imageBlurMaskViewer -> drawBg', 'color: #ccc');

        param = param || {};

        param.offsetX = param.offsetX || defaults.width / 100;
        param.offsetY = param.offsetY || defaults.height / 100;

        param.sx = param.sx || param.offsetX;
        param.sy = param.sy || param.offsetY;
        param.sw = canvas.blur.width - param.offsetX;
        param.sh = canvas.blur.height - param.offsetY;
        param.dx = param.dx || 0;
        param.dy = param.dy || 0;
        param.dw = param.dw || defaults.width;
        param.dh = param.dh || defaults.height;

        context.bg.clearRect(param.dx, param.dy, param.dw, param.dh);

        context.bg.save();

        context.bg.drawImage(canvas.blur, param.sx, param.sy, param.sw, param.sh, param.dx, param.dy, param.dw, param.dh);

        context.bg.restore();
    };

    drawMask = function drawMask(param) {
        console.log('%ctrace: imageBlurMaskViewer -> drawMask', 'color: #ccc');

        param = param || {};

        param.offsetX = param.offsetX || defaults.width / 100;
        param.offsetY = param.offsetY || defaults.height / 100;

        param.sx = param.sx || param.offsetX;
        param.sy = param.sy || param.offsetY;
        param.sw = canvas.bgs.width - param.offsetX;
        param.sh = canvas.bgs.height - param.offsetY;
        param.dx = param.dx || 0;
        param.dy = param.dy || 0;
        param.dw = param.dw || defaults.width;
        param.dh = param.dh || defaults.height;

        context.mask.clearRect(param.dx, param.dy, param.dw, param.dh);

        context.mask.save();

        context.mask.drawImage(images.mask, defaults.lensLeftX, defaults.lensLeftY, defaults.lensWidth, defaults.lensHeight);
        context.mask.globalCompositeOperation = 'source-atop';

        context.mask.mozImageSmoothingEnabled = true;
        context.mask.msImageSmoothingEnabled = true;
        context.mask.imageSmoothingEnabled = true;
        context.mask.drawImage(canvas.bgs, param.sx, param.sy, param.sw, param.sh, param.dx, param.dy, param.dw, param.dh);

        context.mask.restore();
    };

    drawLens = function drawLens(param) {
        console.log('%ctrace: imageBlurMaskViewer -> drawLens', 'color: #ccc');

        param = param || {};

        param.offsetX = param.offsetX || defaults.width / 100;
        param.offsetY = param.offsetY || defaults.height / 100;

        param.sx = param.sx || param.offsetX;
        param.sy = param.sy || param.offsetY;
        param.sw = param.sw || images.lens.width - param.offsetX;
        param.sh = param.sh || images.lens.height - param.offsetY;
        param.dx = param.dx || 0;
        param.dy = param.dy || 0;
        param.dw = param.dw || defaults.width;
        param.dh = param.dh || defaults.height;

        context.lens.clearRect(defaults.lensLeftX, defaults.lensLeftY, defaults.lensWidth, defaults.lensHeight);

        context.lens.save();

        context.lens.drawImage(images.lens, defaults.lensLeftX, defaults.lensLeftY, defaults.lensWidth, defaults.lensHeight);

        context.lens.restore();
    };

    return Module;
});
